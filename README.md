A collector for email-based DMARC reports

It reads the email from a specific IMAP folder and will retrieve the (g)zipped XML-based reports, uncompress them in a target repository.

# How does it work?

Run it with the right options:

```
USAGE:
    dmarccollector [FLAGS] [OPTIONS] --host <HOST> --user <USER>

FLAGS:
    -t, --starttls          whether IMAP should upgrade to TLS once connected (default false)
    -i, --ignoretlserror    ignore certificate errors (invalid hostnames and/or invalid certificates) (default false)
    -h, --help              Prints help information
    -V, --version           Prints version information

OPTIONS:
    -H, --host <HOST>             IMAP host (no default)
    -P, --port <PORT>             IMAP port (default 993)
    -u, --user <USER>             IMAP user (no default)
    -p, --password <PASS_FILE>    path to a file containing the IMAP password (default /usr/local/etc/dmarc-collector)
    -f, --folder <INBOX>          IMAP folder to scan (default INBOX)
    -d, --targetdir <DIR>         target directory to extract the files to (default /var/cache/dmarc-reports)
```

# Quirks

It will mark every mail it parses as read (that’s how IMAP function by default), whether it contains an attachment or not. There is no filtering rules applying to the IMAP client for now so you better have some rules (sieve or equivalent) to put all DMARC emails in one specific IMAP folder.

Has only been tested on Dovecot and Microsoft Exchange for the moment.

