use std::{
    error::Error,
    fmt::{Display, Formatter, Result},
};

/// Representation of errors
#[derive(Debug)]
pub struct DmarcCollectorError {
    details: String,
}

impl DmarcCollectorError {
    pub fn new(msg: &str) -> DmarcCollectorError {
        DmarcCollectorError {
            details: msg.to_string(),
        }
    }
}

impl Display for DmarcCollectorError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.details)
    }
}

impl Error for DmarcCollectorError {}
