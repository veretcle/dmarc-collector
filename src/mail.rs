use imap::{connect, connect_starttls, types::*, Session};
use std::error::Error;

/// Opens a secure IMAP connection and returns it to the main function
pub fn open_secure_imap_connection(
    host: &str,
    port: u16,
    user: &str,
    pass: &str,
    starttls: bool,
    ignoretlserror: bool,
) -> Result<Session<native_tls::TlsStream<std::net::TcpStream>>, Box<dyn Error>> {
    let tls = match ignoretlserror {
        true => native_tls::TlsConnector::builder()
            .danger_accept_invalid_hostnames(true)
            .danger_accept_invalid_certs(true)
            .build()?,
        false => native_tls::TlsConnector::builder().build()?,
    };

    let tls_client = match starttls {
        true => connect_starttls((host, port), host, &tls)?,
        false => connect((host, port), host, &tls)?,
    };

    tls_client.login(user, pass).map_err(|e| e.0.into())
}

/// Returns an u32 vector identifying unread messages in a particular folder
pub fn select_messages(
    session: &mut Session<native_tls::TlsStream<std::net::TcpStream>>,
    folder: &str,
) -> Result<Vec<u32>, Box<dyn Error>> {
    session.select(folder)?;
    Ok(session.search("UNSEEN")?.into_iter().collect())
}

/// Fetch all emails in a vector
pub fn fetch_emails(
    session: &mut Session<native_tls::TlsStream<std::net::TcpStream>>,
    uid: &[u32],
) -> Result<ZeroCopy<Vec<Fetch>>, Box<dyn Error>> {
    Ok(session.fetch(
        uid.iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(","),
        "RFC822",
    )?)
}
