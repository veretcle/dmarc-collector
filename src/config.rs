use clap::{Arg, ArgAction, Command};

/// Configuration structure
pub struct Config {
    /// IMAP parameters
    pub imap_host: String,
    pub imap_port: u16,
    pub imap_user: String,
    pub imap_password: String,
    pub imap_starttls: bool,
    pub imap_folder: String,
    pub imap_ignore: bool,
    /// Where to put output files
    pub targetdir: String,
}

impl Config {
    /// Parses command line to fill up the Config struct
    pub fn new() -> Config {
        let matches = Command::new(env!("CARGO_PKG_NAME"))
            .version(env!("CARGO_PKG_VERSION"))
            .about("A collector for email-based DMARC reports")
            .arg(
                Arg::new("host")
                    .short('H')
                    .long("host")
                    .value_name("HOST")
                    .help("IMAP host")
                    .num_args(1)
                    .required(true),
            )
            .arg(
                Arg::new("port")
                    .short('P')
                    .long("port")
                    .value_name("PORT")
                    .help("IMAP port")
                    .default_value("993")
                    .num_args(1),
            )
            .arg(
                Arg::new("user")
                    .short('u')
                    .long("user")
                    .value_name("USER")
                    .help("IMAP user")
                    .num_args(1)
                    .required(true),
            )
            .arg(
                Arg::new("password")
                    .short('p')
                    .long("password")
                    .value_name("PASS_FILE")
                    .help("path to a file containing the IMAP password")
                    .default_value("/usr/local/etc/dmarc-collector")
                    .num_args(1),
            )
            .arg(
                Arg::new("starttls")
                    .short('t')
                    .long("starttls")
                    .help("whether IMAP should upgrade to TLS once connected")
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("folder")
                    .short('f')
                    .long("folder")
                    .value_name("INBOX")
                    .help("IMAP folder to scan")
                    .default_value("INBOX")
                    .num_args(1),
            )
            .arg(
                Arg::new("ignoretlserror")
                    .short('i')
                    .long("ignoretlserror")
                    .help(
                        "ignore certificate errors (invalid hostnames and/or invalid certificates)",
                    )
                    .action(ArgAction::SetTrue),
            )
            .arg(
                Arg::new("targetdir")
                    .short('d')
                    .long("targetdir")
                    .value_name("DIR")
                    .help("target directory to extract the files to")
                    .default_value("/var/cache/dmarc-reports")
                    .num_args(1),
            )
            .get_matches();

        let imap_path_file = matches
            .get_one::<String>("password")
            .expect("Password file cannot be empty!")
            .to_owned();
        let imap_password = std::fs::read_to_string(imap_path_file)
            .expect("Cannot read password from file!")
            .trim()
            .to_owned();

        Config {
            imap_host: matches.get_one::<String>("host").unwrap().to_owned(),
            imap_port: *matches.get_one::<u16>("port").expect("Port can’t be empty"),
            imap_user: matches.get_one::<String>("user").unwrap().to_owned(),
            imap_password,
            imap_starttls: matches.get_flag("starttls"),
            imap_folder: matches
                .get_one::<String>("folder")
                .expect("Folder cannot be empty!")
                .to_owned(),
            imap_ignore: matches.get_flag("ignoretlserror"),
            targetdir: matches
                .get_one::<String>("targetdir")
                .expect("Targetdir cannot be empty!")
                .to_owned(),
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}
