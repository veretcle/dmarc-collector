mod config;
mod error;
pub use config::Config;
mod attachments;
use attachments::*;
mod mail;
use mail::*;

/// Mail logic function (this is where the magic’s happening)
pub fn run(config: Config) {
    // Open the IMAP session
    let mut imap_session = open_secure_imap_connection(
        &config.imap_host,
        config.imap_port,
        &config.imap_user,
        &config.imap_password,
        config.imap_starttls,
        config.imap_ignore,
    )
    .unwrap_or_else(|e| {
        panic!(
            "Cannot authenticate to IMAP server {} port {} with user {}: {}",
            config.imap_host, config.imap_port, config.imap_user, e
        )
    });

    // Get the IDs of the unread messages in the folder in the mailbox
    let unread_messages: Vec<u32> = select_messages(&mut imap_session, &config.imap_folder)
        .unwrap_or_else(|e| panic!("Unable to open folder {}: {}", config.imap_folder, e));

    // if no new message is get, exit there
    if unread_messages.is_empty() {
        println!("No unseen messages to parse, exiting…");
        return;
    }

    // fetch messages from the IMAP session. Panics if we can’t fetch them.
    let fetched_messages = if let Ok(m) = fetch_emails(&mut imap_session, &unread_messages) {
        m
    } else {
        panic!("Cannot fetch emails from the IMAP Session");
    };

    // get all attachments for every messages in a single attachment vector
    let mut attachments: Vec<Attachment> = vec![];

    for message in fetched_messages.iter() {
        match get_attachments(message) {
            Ok(mut a) => attachments.append(&mut a),
            Err(e) => {
                // in case of error getting the attachments, just print an error message and get to
                // the next message
                println!("Cannot get attachments for message {:?}: {}", message, e);
            }
        };
    }

    // decode the attachments
    attachments.iter_mut().for_each(|x| {
        x.decode()
            .unwrap_or_else(|e| println!("Cannot uncompress attachment {}: {}", &x.filename, e))
    });

    // write them to disk
    attachments.iter().for_each(|x| {
        x.write(&config.targetdir).unwrap_or_else(|e| {
            println!(
                "Cannot write file to target directory {}: {}",
                &config.targetdir, e
            )
        })
    });
}
