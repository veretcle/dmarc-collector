use crate::error::DmarcCollectorError;

use flate2::bufread::GzDecoder;
use imap::types::*;
use mailparse::{parse_mail, DispositionType};
use std::{error::Error, ffi::OsStr, fs::File, io::prelude::*, io::Cursor, path::Path};
use zip::ZipArchive;

/// Enum representing the compression type
#[derive(PartialEq, Eq, Debug)]
pub enum CompressionType {
    Gzip,
    Zip,
    FromFileName,
}

/// Structure representing attachments
pub struct Attachment {
    pub filename: String,
    filetype: CompressionType,
    content: Vec<u8>,
}

impl Attachment {
    /// Completely decodes the part of the Attachment struct
    pub fn decode(&mut self) -> Result<(), Box<dyn Error>> {
        self.find_compression_type()?;

        match self.filetype {
            CompressionType::Gzip => {
                let mut decoder = GzDecoder::new(&self.content[..]);
                let mut s = String::new();
                decoder.read_to_string(&mut s)?;
                self.content = s.as_bytes().to_vec();
            },
            CompressionType::Zip => {
                let reader = Cursor::new(&self.content[..]);
                let mut zip = ZipArchive::new(reader)?;

                if zip.len() != 1 {
                    return Err(Box::new(DmarcCollectorError::new("Archive does not contain exactly 1 element")));
                } else {
                    let file = zip.by_index(0)?;
                    self.content = file.bytes().flatten().collect();
                }
            },
            _ => return Err(Box::new(DmarcCollectorError::new("Error when trying to detect the mimetype of this attachment (CompressionType is still FromFileName"))),
        };

        Ok(())
    }

    /// Find the CompressionType from the filename
    fn find_compression_type(&mut self) -> Result<(), Box<dyn Error>> {
        if let CompressionType::FromFileName = self.filetype {
            // filetype was not explicit (application/octetstream or other non-sense) so we need to
            // modify the object to reflect the right compression type
            let extension = Path::new(&self.filename)
                .extension()
                .and_then(OsStr::to_str)
                .ok_or_else(|| {
                    Box::new(DmarcCollectorError::new(
                        "Cannot determine filename extension",
                    ))
                })?;

            match extension {
                "gz" => self.filetype = CompressionType::Gzip,
                "zip" => self.filetype = CompressionType::Zip,
                _ => {
                    return Err(Box::new(DmarcCollectorError::new(
                        "Attached file is not ZIP type nor GZIP",
                    )))
                }
            }
        }

        Ok(())
    }

    /// Writes the decoded Attachment to disk
    pub fn write(&self, dir: &str) -> Result<(), Box<dyn Error>> {
        let mut targetfile = Path::new(dir).join(&self.filename);
        targetfile = match self.filetype {
            CompressionType::Gzip => targetfile.with_extension(""),
            CompressionType::Zip => targetfile.with_extension("xml"),
            _ => {
                return Err(Box::new(DmarcCollectorError::new(&format!(
                    "Cannot format filename with {} {}",
                    dir, self.filename
                ))))
            }
        };

        let mut file = File::create(&targetfile)?;
        file.write_all(&self.content)?;

        Ok(())
    }
}

/// Determines if the mimetype of the mail part is Gzip, Zip or unknown
fn is_zip_or_gzip(mimetype: &str) -> CompressionType {
    match mimetype {
        "application/gzip" => CompressionType::Gzip,
        "application/zip" | "application/x-zip-compressed" => CompressionType::Zip,
        _ => CompressionType::FromFileName,
    }
}

/// Determines if the mail part is attachment or not
fn is_attachment(content: &mailparse::ParsedContentDisposition) -> bool {
    match content.disposition {
        DispositionType::Attachment => true,
        DispositionType::Inline => {
            if content.params.contains_key("filename") || content.params.contains_key("filename*0")
            {
                return true;
            }
            false
        }
        _ => false,
    }
}

/// Determines if the mail is multipart or not
fn is_multipart(mimetype: &str) -> bool {
    mimetype == "multipart/mixed"
}

/// Extract the parsed email body
fn extract_email_body(message: &Fetch) -> Result<mailparse::ParsedMail, Box<dyn Error>> {
    let body_message = message
        .body()
        .ok_or("Cannot retrieve email body from IMAP fetch struct")?;

    let parsed_email = parse_mail(body_message)?;

    Ok(parsed_email)
}

/// Forms Attachment struct from Email and Content
fn get_attachment(parsed_mail: &mailparse::ParsedMail) -> Result<Attachment, Box<dyn Error>> {
    let parsed_content = parsed_mail.get_content_disposition();
    let filename: String;
    // determine the filename
    if parsed_content.params.contains_key("filename") {
        filename = parsed_content.params["filename"].clone();
    } else if parsed_content.params.contains_key("filename*0") {
        filename = format!(
            "{}{}",
            &parsed_content.params["filename*0"], &parsed_content.params["filename*1"]
        );
    } else {
        return Err(Box::new(DmarcCollectorError::new(
            format!(
                "Cannot determine from ContentDisposition params: {:?}",
                &parsed_content.params
            )
            .as_str(),
        )));
    }

    let filetype = is_zip_or_gzip(&parsed_mail.ctype.mimetype);

    let content = parsed_mail.get_body_raw()?;

    Ok(Attachment {
        filename,
        filetype,
        content,
    })
}

/// Gets all the attachments from a fetched message
pub fn get_attachments(message: &Fetch) -> Result<Vec<Attachment>, Box<dyn Error>> {
    let mut attachments: Vec<Attachment> = vec![];

    let parsed_email = extract_email_body(message)?;

    // determine if mail is multipart or not
    if !is_multipart(&parsed_email.ctype.mimetype) {
        // mail is probably directy the attachment
        if is_attachment(&parsed_email.get_content_disposition()) {
            attachments.push(get_attachment(&parsed_email)?);
        }
    } else {
        // is multipart
        attachments = parsed_email
            .subparts
            .into_iter()
            .filter(|x| is_attachment(&x.get_content_disposition()))
            .flat_map(|x| get_attachment(&x))
            .collect();
    }

    Ok(attachments)
}

#[cfg(test)]
mod tests {
    use super::*;

    use mailparse::ParsedContentDisposition;
    use std::{
        collections::BTreeMap,
        fs::{create_dir_all, read_to_string, remove_dir_all},
    };

    const FILE_CONTENT: &'static str = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";

    #[test]
    fn test_is_multipart() {
        assert!(is_multipart("multipart/mixed"));
        assert!(!is_multipart("tamerolol"));
    }

    #[test]
    fn test_is_zip_or_gzip() {
        assert_eq!(is_zip_or_gzip("application/gzip"), CompressionType::Gzip);
        assert_eq!(is_zip_or_gzip("application/zip"), CompressionType::Zip);
        assert_eq!(
            is_zip_or_gzip("application/x-zip-compressed"),
            CompressionType::Zip
        );
        assert_eq!(
            is_zip_or_gzip("application/tamerelol"),
            CompressionType::FromFileName
        );
    }

    #[test]
    fn test_is_attachment() {
        let a_attachment = ParsedContentDisposition {
            disposition: DispositionType::Attachment,
            params: BTreeMap::from([("tamere".to_string(), "lol".to_string())]),
        };

        assert!(is_attachment(&a_attachment));

        let a_inline_filename = ParsedContentDisposition {
            disposition: DispositionType::Inline,
            params: BTreeMap::from([("filename".to_string(), "tamere.lol".to_string())]),
        };

        assert!(is_attachment(&a_inline_filename));

        let a_inline_filename0 = ParsedContentDisposition {
            disposition: DispositionType::Inline,
            params: BTreeMap::from([("filename*0".to_string(), "tamere.lol".to_string())]),
        };

        assert!(is_attachment(&a_inline_filename0));

        let a_formdata = ParsedContentDisposition {
            disposition: DispositionType::FormData,
            params: BTreeMap::from([("tamere".to_string(), "lol".to_string())]),
        };

        assert!(!is_attachment(&a_formdata));

        let a_inline_not_filename = ParsedContentDisposition {
            disposition: DispositionType::Inline,
            params: BTreeMap::from([("tamere".to_string(), "lol".to_string())]),
        };

        assert!(!is_attachment(&a_inline_not_filename));
    }

    #[test]
    fn test_find_compression_type() {
        let mut t1 = Attachment {
            filename: "tarace.zip".to_string(),
            filetype: CompressionType::FromFileName,
            content: vec![],
        };

        t1.find_compression_type().unwrap();

        assert_eq!(t1.filetype, CompressionType::Zip);

        let mut t2 = Attachment {
            filename: "youpi.gz".to_string(),
            filetype: CompressionType::FromFileName,
            content: vec![],
        };

        t2.find_compression_type().unwrap();

        assert_eq!(t2.filetype, CompressionType::Gzip);
    }

    #[test]
    #[should_panic]
    fn test_find_compression_type_none() {
        let mut t = Attachment {
            filename: "tamere.lol".to_string(),
            filetype: CompressionType::FromFileName,
            content: vec![],
        };

        t.find_compression_type().unwrap();
    }

    #[test]
    fn test_attachment_write_gz() {
        const TEST_DIR: &'static str = "/tmp/dmarccollector_test_attachment_write_gz";
        const FILE_NAME: &'static str = "youpi.test.youpla.xml";

        create_dir_all(TEST_DIR).unwrap();

        let mut t = Attachment {
            filename: format!("{}.gz", FILE_NAME),
            filetype: CompressionType::Gzip,
            // encoded gzip
            content: vec![
                31, 139, 8, 0, 0, 0, 0, 0, 0, 3, 179, 177, 175, 200, 205, 81, 40, 75, 45, 42, 206,
                204, 207, 179, 85, 50, 212, 51, 80, 82, 72, 205, 75, 206, 79, 201, 204, 75, 183,
                85, 10, 13, 113, 211, 181, 80, 82, 176, 183, 227, 2, 0, 91, 138, 44, 241, 40, 0, 0,
                0,
            ],
        };

        t.decode().unwrap();
        t.write(TEST_DIR).unwrap();

        let path = Path::new(TEST_DIR).join(FILE_NAME);
        assert!(path.is_file());

        let file_content = read_to_string(path).unwrap();
        assert_eq!(FILE_CONTENT, file_content);

        remove_dir_all(TEST_DIR).unwrap();
    }

    #[test]
    fn test_attachment_write_zip() {
        const TEST_DIR: &'static str = "/tmp/dmarccollector_test_attachment_write_zip";
        const FILE_NAME: &'static str = "youpla.test.youpi";

        create_dir_all(TEST_DIR).unwrap();

        let mut t = Attachment {
            filename: format!("{}.zip", FILE_NAME),
            filetype: CompressionType::Zip,
            // hardcoded zip file
            content: vec![
                80, 75, 3, 4, 10, 0, 0, 0, 0, 0, 44, 135, 162, 84, 91, 138, 44, 241, 40, 0, 0, 0,
                40, 0, 0, 0, 21, 0, 28, 0, 121, 111, 117, 112, 108, 97, 46, 116, 101, 115, 116, 46,
                121, 111, 117, 112, 105, 46, 120, 109, 108, 85, 84, 9, 0, 3, 84, 241, 111, 98, 86,
                241, 111, 98, 117, 120, 11, 0, 1, 4, 232, 3, 0, 0, 4, 217, 3, 0, 0, 60, 63, 120,
                109, 108, 32, 118, 101, 114, 115, 105, 111, 110, 61, 34, 49, 46, 48, 34, 32, 101,
                110, 99, 111, 100, 105, 110, 103, 61, 34, 85, 84, 70, 45, 56, 34, 32, 63, 62, 10,
                80, 75, 1, 2, 30, 3, 10, 0, 0, 0, 0, 0, 44, 135, 162, 84, 91, 138, 44, 241, 40, 0,
                0, 0, 40, 0, 0, 0, 21, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 164, 129, 0, 0, 0, 0, 121,
                111, 117, 112, 108, 97, 46, 116, 101, 115, 116, 46, 121, 111, 117, 112, 105, 46,
                120, 109, 108, 85, 84, 5, 0, 3, 84, 241, 111, 98, 117, 120, 11, 0, 1, 4, 232, 3, 0,
                0, 4, 217, 3, 0, 0, 80, 75, 5, 6, 0, 0, 0, 0, 1, 0, 1, 0, 91, 0, 0, 0, 119, 0, 0,
                0, 0, 0,
            ],
        };

        t.decode().unwrap();
        t.write(TEST_DIR).unwrap();

        let path = Path::new(TEST_DIR).join(&format!("{}.xml", FILE_NAME));
        assert!(path.is_file());

        let file_content = read_to_string(path).unwrap();
        assert_eq!(FILE_CONTENT, file_content);

        remove_dir_all(TEST_DIR).unwrap();
    }
}
