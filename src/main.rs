use dmarccollector::*;

fn main() {
    let config = Config::new();

    run(config);
}
